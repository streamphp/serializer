<?php

/**
 * This File is part of the Stream\Serializer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Serializer;

/**
 * Interface Parser
 *
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
interface InterfaceParser
{
    /**
     * decode
     *
     * @param mixed $data
     * @access public
     * @return mixed
     */
    public function decode($data);

    /**
     * encode
     *
     * @param mixed $data
     * @access public
     * @return mixed
     */
    public function encode($data);
}
