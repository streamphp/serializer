<?php

/**
 * This File is part of the Stream\Serializer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Serializer\Processors;

use \Closure;
use \DOMNode;
use \DOMDocument;
//use \SimpleXMLElement;
use Stream\Serializer\SimpleXMLElement;
use Stream\Serializer\InterfaceParser;

/**
 * Class: XMLProcessor
 *
 * @implements InterfaceParser
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class XMLProcessor implements InterfaceParser
{
    /**
     * dom
     *
     * @var DOMDocument
     */
    protected $dom;

    /**
     * boolean
     *
     * @var array
     */
    protected static $boolean = ['yes','no', 'true', 'false'];

    /**
     * singulars
     *
     * @var mixed
     */
    protected $singulars = false;

    /**
     * singularizer
     *
     * @var Closure
     */
    protected $singularizer;

    /**
     * pluralizer
     *
     * @var Closure
     */
    protected $pluralizer;

    /**
     * plurals
     *
     * @var array
     */
    protected $plurals = false;

    /**
     * rootName
     *
     * @var string
     */
    protected $rootName = 'data';

    /**
     * checkPrefixes
     *
     * @var boolean
     */
    protected $checkPrefixes;

    /**
     * namespaceSeparator
     *
     * @var string
     */
    protected $prefixSeparator;

    /**
     * __construct
     *
     * @param mixed $rootName
     */
    public function __construct($rootName = null, $checkPrefixes = true, $prefixSeparator = ':')
    {
        $this->rootName        = $rootName ? $rootName : $this->rootName;
        $this->checkPrefixes   = $checkPrefixes;
        $this->prefixSeparator = $prefixSeparator;
    }

    /**
     * encode
     *
     * @param mixed $data
     * @access public
     * @return string
     */
    public function encode($data)
    {
        $this->dom = new DOMDocument();

        $xmlRoot = $this->rootName;
        $root = $this->dom->createElement($xmlRoot);
        $this->dom->appendChild($root);

        $this->buildXML($root, $data);

        return $this->dom->saveXML();
    }

    /**
     * decode
     *
     * @param mixed $data
     * @access public
     * @return array
     */
    public function decode($data)
    {
        // store current libxml setting:
        $usedInternalErrors = libxml_use_internal_errors(true);
        $externalEntitiesDisabled = libxml_disable_entity_loader(true);
        libxml_clear_errors();

        $dom = new DOMDocument();
        // LIBXML_NONET prevents local and remote file inclusion attacks
        $dom->loadXML($data, LIBXML_NONET | LIBXML_DTDATTR | defined('LIBXML_COMPACT') ? LIBXML_COMPACT : 0);

        // restore previous libxml setting:
        libxml_use_internal_errors($usedInternalErrors);
        libxml_disable_entity_loader($externalEntitiesDisabled);

        $dom->normalizeDocument();
        $xmlObj = simplexml_import_dom($dom, 'Stream\\Serializer\\SimpleXMLElement');
        $namespaces = $xmlObj->getNamespaces();
        $root = key($namespaces) !== '' ? $this->prefixKey(key($namespaces), $xmlObj->getName()) : $xmlObj->getName();
        $data = $this->parseXML($xmlObj, $namespaces);

        return array($root => $data);
    }

    /**
     * childNodesToArray
     *
     * @param array $children array containing SimpleXMLElements, most likely
     *  derived from an xpath query
     * @param string $parentName local-name of the parent node
     * @access public
     * @return array
     */
    public function childNodesToArray($children, $parentName = null, $nestedValues = false)
    {
        $result = [];
        foreach ($children as $child) {

            if (!$this->isSimpleXMLElement($child)) {
                throw new InvalidArgumentException(sprintf('The input array must only contain SimpleXMLElements but contains %s', gettype($child)));
            }

            $localNamespaces = $child->getNamespaces();
            $prefix = key($localNamespaces);
            $prefix = strlen($prefix) ? $prefix : null;
            $nsURL = current($localNamespaces);

            $name = $child->getName();
            $oname = $name;
            $name = is_null($prefix) ? $name : $this->prefixKey($prefix, $name);

            if (count($children) < 2) {
                $result[$name] = $this->parseXML($child, $nestedValues);
                break;
            }

            if (isset($result[$name])) {
                if (is_array($result[$name]) && array_numeric($result[$name])) {
                    $value = $this->parseXML($child, $nsURL, $prefix);
                    if (is_array($value) && array_numeric($value)) {
                        $result[$name] = array_merge($result[$name], $value);
                    } else {
                        $result[$name][] = $value;
                    }
                } else {
                    continue;
                }
            } else {

                $equals = $this->getEqualNodes($child, $prefix);

                if (count($equals) > 1) {
                    if ($this->isEqualOrPluralOf($parentName, $oname)) {
                        $result[] = $this->parseXML($child, $nestedValues);
                    } else {
                        $plural = $this->pluralize($oname);
                        $plural = is_null($prefix) ? $plural : $this->prefixKey($prefix, $plural);
                        if (isset($result[$plural]) && is_array($result[$plural])) {
                            $result[$plural][] = $this->parseXML($child, $nestedValues);
                        } elseif (count($children) !== count($equals)) {
                            $result[$plural][] = $this->parseXML($child, $nestedValues);
                        } else {
                            $result[$name][] = $this->parseXML($child, $nestedValues);
                        }
                    }
                } else {
                    $result[$name] = $this->parseXML($child, $nsURL, $nestedValues);
                }
            }
        }
        return $result;
    }
    /**
     * setPluralizer
     *
     * @param mixed $
     * @param mixed $pluralizer
     * @access public
     * @return void
     */
    public function setPluralizer(Closure $pluralizer)
    {
        $this->plurals = true;
        $this->pluralizer = $pluralizer;
    }

    /**
     * setSingularizer
     *
     * @param mixed $
     * @param mixed $singularizer
     * @access public
     * @return void
     */
    public function setSingularizer(Closure $singularizer)
    {
        $this->singulars = true;
        $this->singularizer = $singularizer;
    }

    /**
     * pluralize
     *
     * @param mixed $value
     * @access protected
     * @return mixed
     */
    protected function pluralize($value)
    {
        if (!$this->plurals) {
            return $value;
        }
        $fn = $this->pluralizer;
        return $fn($value);

    }

    /**
     * singularize
     *
     * @param mixed $value
     * @access protected
     * @return mixed
     */
    protected function singularize($value)
    {
        if (!$this->singulars) {
            return $value;
        }
        $fn = $this->singularizer;
        return $fn($value);
    }


    /**
     * simplexmlInsertAfter
     *
     * @param SimpleXMLElement $insert
     * @param SimpleXMLElement $target
     * @access public
     * @return mixed
     */
    public function simplexmlInsertAfter(SimpleXMLElement &$insert, SimpleXMLElement &$target)
        {
        $target_dom = dom_import_simplexml($target);
        $insert_dom = $target_dom->ownerDocument->importNode(dom_import_simplexml($insert), true);
        if ($target_dom->nextSibling) {
            return $target_dom->parentNode->insertBefore($insert_dom, $target_dom->nextSibling);
        } else {
            return $target_dom->parentNode->appendChild($insert_dom);
        }
    }

    /**
     * isSimpleXMLElement
     *
     * @param mixed $element
     * @access public
     * @return mixed
     */
    public function isSimpleXMLElement($element)
    {
        return $element instanceof SimpleXMLElement;
    }

    /**
     * isTraversable
     *
     * @param mixed $data
     * @access public
     * @return mixed
     */
    public function isTraversable($data)
    {
        return is_array($data) || $data instanceof \Traversable;
    }

    /**
     * buildXML
     *
     * @param mixed $root
     * @param mixed $data
     */
    protected function buildXML(DOMNode $DOMNode, $data)
    {
        $isIndexedArray = ctype_digit(implode('', array_keys($data)));
        $hasAttributes = false;

        if ($this->isTraversable($data)) {

            foreach ($data as $key => $value) {

                if (strpos($key, '@') === 0 && $this->isValidNodeName($attrName = substr($key, 1))) {
                    $hasAttributes = true;

                    if (is_array($value)) {
                        foreach ($value as $attrKey => $attrValue) {
                            $DOMNode->setAttribute($attrKey, $attrValue);
                        }
                    } else {
                        $DOMNode->setAttribute($attrName, (string)$value);
                    }
                    continue;
                }

                if (is_array($value) && !is_int($key)) {
                    if (array_numeric($value)) {
                        foreach ($value as $arrayValue) {
                            $this->appendDOMNode($DOMNode, $this->singularize($key), $arrayValue);
                        }
                        continue;
                    }
                } else if (is_int($key) || !$this->isValidNodeName($key)) {
                    $key = 'item';
                }

                if ($this->isValidNodeName($key)) {
                    $this->appendDOMNode($DOMNode, $key, $value, $hasAttributes);
                }
            }

        } else if (is_object($data)) {

        }

        return true;
    }

    /**
     * parseXML
     *
     * @param SimpleXMLElement $xml
     * @access protected
     * @return array
     */
    protected function parseXML(SimpleXMLElement $xml, $nestedValues = true)
    {
        $childpool  = $xml->xpath('child::*');
        $attributes = $xml->xpath('./@*');
        $parentName = $xml->getName();


        if (!empty($attributes)) {
            $attrs = [];
            foreach ($attributes as $key => $attribute) {
                $namespaces = $attribute->getnameSpaces();
                $value = $attribute->phpValue();
                if ($prefix = $this->nsPrefix($namespaces)) {
                    $attName = $this->prefixKey($prefix, $attribute->getName());
                } else {
                    $attName  = $attribute->getName();
                }
                $attrs[$attName] = $value;
            }
            $attributes = ['@attributes' => $attrs];
        }

        $text = $this->prepareTextValue($xml, current($attributes));
        $result = $this->childNodesToArray($childpool, $parentName);

        if (!empty($attributes)) {
            if (!is_null($text)) {
                $result[$this->getTypeKey($text)] = $text;
            }
            $result = array_merge($attributes, $result);
            return $result;

        } else if (!is_null($text)) {
            if (!empty($result)) {
                $result[$this->getTypeKey($text)] = $text;
            } else {
                $result = $text;
            }
            return $result;
        }
        return (empty($result) && is_null($text)) ? null : $result;
    }

    /**
     * setElementValue
     *
     * @param DOMNode $DOMNode
     * @param mixed $value
     */
    protected function setElementValue($DOMNode, $value = null)
    {
        switch (true) {
            case $value instanceof \SimpleXMLElement:
                $node = dom_import_simplexml($value);
                $this->dom->importNode($node);
                $DOMNode->appendChild($node);
                break;
            case $value instanceof \DOMNode:
                $DOMNode->appendChild($value);
                break;
            case is_array($value) || $value instanceof \Traversable:
                return $this->buildXML($DOMNode, $value);
            case is_numeric($value):
                if (is_string($value)) {
                    return $this->createTextNodeWithTypeAttribute($DOMNode, (string)$value, 'string');
                }
                return $this->createText($DOMNode, (string)$value);
            case is_bool($value):
                return $this->createText($DOMNode, $value ? 'yes' : 'no');
            case is_string($value):
                if (preg_match('/(<|>|&)/i', $value)) {
                    return $this->createCDATASection($DOMNode, $value);
                }
                return $this->createText($DOMNode, $value);
            default:
                return $value;
        }
    }

    /**
     * getElementValue
     *
     * @param string $value
     * @access protected
     * @return mixed
     */
    protected function getElementValue($value)
    {
        return $value;
    }

    /**
     * isValidNodeName
     *
     * @param mixed $name
     * @access protected
     * @return boolean
     */
    protected function isValidNodeName($name)
    {
        return !empty($name) && false === strpos($name, ' ') && preg_match('#^[\pL_][\pL0-9._-]*$#ui', $name);
    }

    /**
     * isEqualOrPluralOf
     *
     * @param mixed $name
     * @param mixed $singular
     * @access protected
     * @return boolean
     */
    protected function isEqualOrPluralOf($name, $singular)
    {
        return $name === $singular || $name === $this->pluralize($singular);
    }

    /**
     * getEqualNodes
     *
     * @param SimpleXMLElement $node
     * @param mixed $prefix
     * @access protected
     * @return array
     */
    protected function getEqualNodes(SimpleXMLElement $node, $prefix = null)
    {
        $name = is_null($prefix) ? $node->getName() : sprintf("%s:%s", $prefix, $node->getName());
        return $node->xpath(
            sprintf(".|following-sibling::*[name() = '%s']|preceding-sibling::*[name() = '%s']", $name, $name)
        );
    }
    /**
     * getEqualFollowingNodes
     *
     * @param SimpleXMLElement $node
     * @param mixed $prefix
     * @access protected
     * @return array
     */
    protected function getEqualFollowingNodes(SimpleXMLElement $node, $prefix = null)
    {
        $name = is_null($prefix) ? $node->getName() : sprintf("%s:%s", $prefix, $node->getName());
        return $node->xpath(
            sprintf(".|following-sibling::*[name() = '%s']", $name)
        );
    }

    /**
     * simpleXMLParentElement
     *
     * @param SimpleXMLElement $element
     * @param int $maxDepth
     * @access protected
     * @return boolean|SimpleXMLElement
     */
    protected function simpleXMLParentElement(SimpleXMLElement $element, $maxDepth = 4)
    {
        if (!$parent = current($element->xpath('parent::*'))) {
            $xpath = '';
            while ($maxDepth--) {
                $xpath .= '../';
                $query = sprintf('%sparent::*', $xpath);

                if ($parent = current($element->xpath($query))) {
                    return $parent;
                }

            }
        }
        return $parent;
    }

    /**
     * prefixKey
     *
     * @param mixed $prefix
     * @param mixed $localName
     * @access protected
     * @return mixed
     */
    protected function prefixKey($prefix, $localName)
    {
        if (!$this->checkPrefixes) {
            return $localName;
        }
        return sprintf('%s%s%s', $prefix, $this->prefixSeparator, $localName);
    }

    /**
     * simpleXMLNextSibling
     *
     * @param SimpleXMLElement $element
     * @access protected
     * @return mixed
     */
    protected function simpleXMLNextSibling(SimpleXMLElement $element)
    {
        if ($sibling = $element->xpath('following-sibling::*')) {
            return current($sibling);
        }
    }

    /**
     * nsPrefix
     *
     * @param array $namespaces
     * @access protected
     * @return mixed
     */
    protected function nsPrefix(array $namespaces)
    {
        $prefix = key($namespaces);
        return strlen($prefix) ? $prefix : null;
    }



    /**
     * convert boolish and numeric values
     *
     * @param mixed $text
     * @param array $attributes
     */
    protected function prepareTextValue(SimpleXMLElement $xml, $attributes = null)
    {
        return (isset($attributes['type']) && 'text' === $attributes['type']) ? clear_value((string)$xml) : $xml->phpValue();
    }

    /**
     * determine the array key name for textnodes with attributes
     *
     * @param mixed|string $value
     */
    protected function getTypeKey($value)
    {
        return is_string($value) ? 'text' : 'value';
    }

    /**
     * appendDOMNode
     *
     * @param DOMNode $DOMNode
     * @param string  $name
     * @param mixed   $value
     * @param boolean $hasAttributes
     * @access protected
     * @return void
     */
    protected function appendDOMNode($DOMNode, $name, $value = null, $hasAttributes = false)
    {
        $element = $this->dom->createElement($name);

        if ($hasAttributes && ($name === 'text' || $name === 'value')) {
            $this->setElementValue($DOMNode, $value);
        } else if ($this->setElementValue($element, $value)) {
            $DOMNode->appendChild($element);
        }
    }


    /**
     * createCDATASection
     *
     * @param DOMNode $DOMNode
     * @param string  $value
     * @access protected
     * @return boolean
     */
    protected function createCDATASection($DOMNode, $value)
    {
        $cdata = $this->dom->createCDATASection($value);
        $DOMNode->appendChild($cdata);
        return true;
    }

    /**
     * createText
     *
     * @param DOMNode $DOMNode
     * @param string  $value
     * @access protected
     * @return boolean
     */
    protected function createText($DOMNode, $value)
    {
        $text = $this->dom->createTextNode($value);
        $DOMNode->appendChild($text);
        return true;
    }

    /**
     * createTextNodeWithTypeAttribute
     *
     * @param DOMNode $DOMNode
     * @param mixed   $value
     * @param string  $type
     * @access protected
     * @return booelan
     */
    protected function createTextNodeWithTypeAttribute($DOMNode, $value, $type = 'int')
    {
        $text = $this->dom->createTextNode($value);
        $attr = $this->dom->createAttribute('type');
        $attr->value = $type;
        $DOMNode->appendChild($text);
        $DOMNode->appendChild($attr);
        return true;
    }
}
